# <!-- coding: utf-8 -->
#
# la partie couches : vecteur et raster
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# les couches ogc : récupération sur internet
# source("geo/scripts/wetlands.R");couches_ogc_ecrire()
couches_ogc_ecrire <- function(nc, test=2) {
  carp()
  library(tidyverse)
  library(sf)
  couches_images_lire()
  ogcDir <- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  imgs <- vector()
  n <- nrow(nc)
  for ( i in 1:n ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
    parcours <- nc[[i, 'id']]
    carp("i: %s/%s parcours: %s", i, n, parcours)
    dsn <- sprintf("%s/%s_carte.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'WMTS', 'carte', marge=500, size_max=1024)
    }
    dsn <- sprintf("%s/%s_satellite.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'WMTS', 'satellite', size_max=1024)
    }
    next;
    dsn <- sprintf("%s/%s_osm_tous_fr.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'TMS', 'osm_tous_fr', marge=500, size_max=1024)
    }
    next;
    dsn <- sprintf("%s/%s_sc1000.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'crop', 'sc1000', marge=20000, size_max=1024)
    }

    next;
    dsn <- sprintf("%s/%s_gmses13.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'crop', 'gmses13', marge=8000, size_max=1024)
    }
    next
    dsn <- sprintf("%s/%s_carte_8k.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'WMTS', 'carte', marge=8000, size_max=1024)
    }
    next



    dsn <- sprintf("%s/%s_ign_scan.tif", ogcDir, parcours)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_dl_bbox(nc1, dsn, 'WMS', 'ign:ign_scan')
    }
  }
}
couches_sites_lire <- function() {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/elementaires.geojson", varDir)
  carp('dsn: %s', dsn)
  sf <- st_read(dsn) %>%
    st_transform(2154) %>%
    filter(! grepl('/', name)) %>%
#    dplyr::select(-dsn) %>%
    arrange(name) %>%
    mutate(dsn=camel5(name)) %>%
    glimpse()
  return(invisible(sf))
}
#
# lecture des fonds de carte
couches_images_lire <- function() {
  library(raster)
  carp()
  if ( ! exists("imgGMSES10") ) {
    rasterFic <- sprintf("%s/couches/35_GP_GMSES_10_2154.tif", varDir)
    carp("rasterFic:%s", rasterFic)
    imgGMSES10 <<- brick(rasterFic)
  }
  if ( ! exists("imgGMSES13") ) {
    rasterFic <- sprintf("%s/couches/35_GP_GMSES_13_2154.tif", varDir)
    carp("rasterFic:%s", rasterFic)
    imgGMSES13 <<- brick(rasterFic)
  }
  if ( ! exists("imgSC1000") ) {
    rasterFic <- sprintf("%s/SC1000/SC1000_0050_7130_L93_E100.jp2", ignDir)
    carp("rasterFic:%s", rasterFic)
    imgSC1000 <<- brick(rasterFic)
  }
}

