# wetlands : traitement des sites wetlands

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des donn�es en provenance d'OpenStreetMap et des fichiers SIG Bretagne Vivante et GONm.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilis� pour la production des documents.

MikTex est l'environnement utilis�.

Les fichiers .tex sont dans le dossier WETLANDS.
